#! /bin/bash
# Install Apache
echo "Installing Apache"
apt-get install apache2 -y

echo "Installing PHP & various PHP modules"
# Install PHP
apt-get install php5-common php5-dev libapache2-mod-php5 php5-cli php5-fpm -y
apt-get install curl php5-curl php5-gd php5-mcrypt php5-mysql -y
