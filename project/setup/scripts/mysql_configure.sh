#! /bin/bash
# Setup MySQL database and users
# Privileges need to be re-granted and flushed after provisioning
echo "Creating mysql user daas, databases daasdb , and importing database"
echo "Creating MySQL user daas"
mysql -uroot -proot -e "CREATE USER 'daas'@'localhost' IDENTIFIED BY 'daasdbpaw';"
echo "Creating MySQL database daas"
mysql -uroot -proot -e "CREATE DATABASE daasdb"
echo "Granting privileges to new user daas and root"
mysql -uroot -proot -e "grant all privileges on daasdb.* to 'daas'@'localhost' identified by 'daas'"
mysql -uroot -proot -e "grant all privileges on daasdb.* to 'root'@'localhost' identified by 'root'"
echo "Importing daas database dump"
mysql -uroot -proot -e "USE daasdb; SOURCE /var/www/html/setup/files/dump.sql;"

# Change MySQL bind address to allow connections from anywhere
echo "Allowing MySQL connections from anywhere"
sed -i "s/bind-address.*/bind-address = 0.0.0.0/" /etc/mysql/my.cnf

# Restart the SQL service
echo "Restarting MySQL"
service mysql restart
