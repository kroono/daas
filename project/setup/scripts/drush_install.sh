#! /bin/bash
# Install Drush
# This needs to be re-done after provisioning
echo "Installing Drush"
/usr/local/bin/composer global require drush/drush:6.*
echo "Creating an alias of drush to /usr/local/bin"
ln -s /home/vagrant/.composer/vendor/drush/drush/drush /usr/local/bin/drush
echo "Adding drush to PATH"
echo 'export PATH="/home/vagrant/.composer/vendor/drush/drush/drush:$PATH"' >> /home/vagrant/.bashrc
