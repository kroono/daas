#! /bin/bash
# Install Composer
echo "Installing Composer"
curl -sS https://getcomposer.org/installer | php
echo "Moving composer to usr/local/bin"
mv composer.phar /usr/local/bin/composer
echo "Adding composer to PATH"
sed -i '1i export PATH="/home/vagrant/.composer/vendor/bin:$PATH"' /home/vagrant/.bashrc
