#! /bin/bash
TIMEZONE="America/New_York"

echo "Setting timezone..."
echo $TIMEZONE | sudo tee /etc/timezone
sudo dpkg-reconfigure --frontend noninteractive tzdata
